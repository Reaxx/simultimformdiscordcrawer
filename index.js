'use strict';

/**
 * Send a message using a webhook
 */

// Import the discord.js module
const Discord = require('discord.js');
const SimCrawler = require('./simcrawler.js');
require('dotenv').config();


let crawler = new SimCrawler();

Crawl();
setInterval(Crawl, 1*60*1000);

// Create a new webhook
const hook = new Discord.WebhookClient(process.env.WEBHOOK_ID, process.env.WEBHOOK_TOKEN);
// console.log(process.env.WEBHOOK_ID);
// console.log(process.env.WEBHOOK_TOKEN);
// hook.send("test");

function Crawl() {
    crawler.GetPage(SendMsg);
}

function SendMsg(newPost) {
    let msg = "Ny post i forumet.\n" + newPost.url.trim();
    console.log(msg);
    hook.send(msg);
}