const cheerio = require('cheerio');
const request = require('request')
const fs = require('fs');

class SimCrawler {
    constructor(name) {          
        this.URL = "https://simultima.wixsite.com/forum";
        this.OldPost = {};
        this.LastPost = {};
    }

    GetPage(callback) {
        let newestPost = {};
        let self = this;
        request(this.URL, function (err, res, body) {
            if(err)
            {
                console.log(err, "error occured while hitting URL");
            }
            else
            {
                let $ = cheerio.load(body);

                let titleDom = $(".post-title");
                newestPost.title = titleDom.first().text();
                newestPost.url = titleDom.first().parent().attr('href');
                // newestPost.user = $(".LXBdL").first().text();
                newestPost.counter = $(".nltsO").eq(1).text();
                // console.log(newestPost);
                self.CheckIfNew(newestPost, callback);
            }
        });
    }     

    CheckIfNew(newestPost,callback) {
        console.log("Checking: "+newestPost.title+" [" +newestPost.counter+"]");

        // If first run, saves post without calling discord
        if(!this.LastPost.title) {
            this.LastPost=newestPost;
            console.log("First post, wont send messege to discord.");
        }
        // Otherwise checks if new post
        else if(this.IsPostChanged(newestPost))
        {
            console.log("New post");
            this.LastPost=newestPost;
           callback(newestPost);
        }
        else {
            console.log("Old post");
        }

        return false;
    }

    IsPostChanged(newestPost)
    {
        return ((this.LastPost.title != newestPost.title) || (this.LastPost.counter != newestPost.counter));
    }
}


module.exports = SimCrawler;